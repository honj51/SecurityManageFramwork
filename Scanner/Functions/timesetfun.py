# -*- coding: utf-8 -*-
# @Time    : 2020/12/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : timesetfun.py
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from Base.Functions import basefun
from django.utils import timezone
from . import publicfun
import datetime


# 实例化调度器
scheduler = BackgroundScheduler()
# 调度器使用默认的DjangoJobStore()
scheduler.add_jobstore(DjangoJobStore(), 'default')


def test_fun():
    print('test')


def task_plan_create(task_obj):
    id_get = basefun.getuuid(task_obj.id)
    task_obj.key = id_get
    task_obj.save()
    publicfun.task_scan(task_obj, task_obj.user)
    try:
        if task_obj.type.key == 'scan':
            if task_obj.start_time > timezone.localtime():
                time_run = task_obj.start_time.strftime('%Y-%m-%d %H:%M:%S')
            else:
                time_run = (datetime.datetime.now()+datetime.timedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S')
            scheduler.add_job(publicfun.start_scan, 'date', id=id_get, run_date=time_run, args=[task_obj.id])
        elif task_obj.type.key == 'cycle':
            #publicfun.start_cycle_scan(task_obj.id)
            scheduler.add_job(publicfun.start_cycle_scan, 'interval', id=id_get, hours=int(task_obj.cycle_hours), args=[task_obj.id])
        else:
            pass
    except:
        return False
    return True


def task_plan_delete(task_obj):
    try:
        scheduler.remove_job(task_obj.key)
        return True
    except:
        return False


# 注册定时任务并开始
register_events(scheduler)
scheduler.start()
