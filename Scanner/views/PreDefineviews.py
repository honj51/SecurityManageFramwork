# -*- coding: utf-8 -*-
# @Time    : 2020/7/26
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : PreDefineviews.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_protect
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from Task import forms, models, serializers
from Scanner.models import Policies


@api_view(['GET'])
def predefine_list(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = models.PreDefine.objects.filter(name__icontains=key).order_by('id')
    list_count = list_get.count()
    serializers_get = serializers.PreDefineSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def predefine_create(request):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    form = forms.PredefinForm(request.POST)
    if form.is_valid():
        item_get = models.PreDefine.objects.get_or_create(
            name=form.cleaned_data['name'],
            tasktype=form.cleaned_data['tasktype'],
            key=form.cleaned_data['key']
        )
        if item_get[1]:
            item_get = item_get[0]
            if form.cleaned_data['police']:
                police_list = Policies.objects.filter(
                    id__in=form.cleaned_data['police'].split(',')
                )
                item_get.police.add(*police_list)
            item_get.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '已存在'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def predefine_update(request, predefine_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    item_get = models.PreDefine.objects.filter(id=predefine_id).first()
    if item_get:
        form = forms.PredefinForm(request.POST)
        if form.is_valid():
            item_get.name = form.cleaned_data['name']
            item_get.tasktype = form.cleaned_data['tasktype']
            item_get.key = form.cleaned_data['key']
            item_get.name = form.cleaned_data['name']
            if form.cleaned_data['police']:
                police_list = Policies.objects.filter(
                    id__in=form.cleaned_data['police'].split(',')
                )
                item_get.police.add(*police_list)
            item_get.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def predefine_delete(request, predefine_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    item_get = models.PreDefine.objects.filter(id=predefine_id).first()
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)
