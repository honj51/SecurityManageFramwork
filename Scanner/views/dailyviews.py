# -*- coding: utf-8 -*-
# @Time    : 2020/7/26
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : tasks.py
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from datetime import datetime, timedelta
from Scanner import models as scannermodels
from Scanner import tasks
from ..Functions import publicfun

# 实例化调度器
scheduler = BackgroundScheduler()
# 调度器使用默认的DjangoJobStore()
scheduler.add_jobstore(DjangoJobStore(), 'default')


# 每天0点半执行这个任务
'''
@register_job(scheduler, 'cron', id='d01pbc_sync', hour=0, minute=30)
def d01pbc_sync():
    start_time = (datetime.now() + timedelta(days=-1)).strftime('%Y-%m-%d %H:%M:%S')
    end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    scanner_obj = scannermodels.Scanner.objects.filter(type='D01_pbc', status=True).first()
    tasks.get_d01pbc_sync.delay(scanner_obj.id, start_time, end_time)
    print('success')
'''


# 注册定时任务并开始
register_events(scheduler)
scheduler.start()
