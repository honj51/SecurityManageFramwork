# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import views, policiesviews, scannerviews, PreDefineviews

urlpatterns = [
    path('base/list/scanner/', views.main_list, name='scannerlist'),
    path('select/scanner/', views.select_list, name='select_list'),
    path('list/policies/<str:scanner_id>/', views.police_list, name='police_list'),

    path('base/create/scanner/', scannerviews.scanner_create, name='scanner_create'),
    path('base/update/scanner/<str:scanner_id>/', scannerviews.scanner_update, name='scanner_update'),
    path('base/delete/scanner/<str:scanner_id>/', scannerviews.scanner_delete, name='scanner_delete'),
    path('base/action/scanner/<str:scanner_id>/', scannerviews.scanner_sync, name='scanner_sync'),

    path('base/create/policies/<str:scanner_id>/', policiesviews.policies_create, name='policies_create'),
    path('base/update/policies/<str:police_id>/', policiesviews.policies_update, name='policies_update'),
    path('base/delete/policies/<str:police_id>/', policiesviews.policies_delete, name='policies_delete'),

    path('base/list/predefine/', PreDefineviews.predefine_list, name='predefine_list'),
    path('base/create/predefine/', PreDefineviews.predefine_create, name='predefine_create'),
    path('base/update/predefine/<str:predefine_id>/', PreDefineviews.predefine_update, name='predefine_update'),
    path('base/delete/predefine/<str:predefine_id>/', PreDefineviews.predefine_delete, name='predefine_delete'),
]
