# -*- coding: utf-8 -*-
# @Time    : 2020/6/3
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py
from django.urls import path
from .views import taskviews, typeviews ,vulnviews

urlpatterns = [
    path('task/list/', taskviews.list_views),
    path('task/create/', taskviews.create_views),
    path('task/delete/<str:task_id>/', taskviews.delete_views),
    path('task/update/<str:task_id>/', taskviews.update_views),

    path('vuln/list/<str:task_id>/', vulnviews.list_views),

    path('type/select/', typeviews.select_type_views),
    path('predefine/select/<str:type_id>/', typeviews.select_predefine_views),
]