# coding:utf-8

# Create your views here.
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from django.db.models import Q
from .. import models, serializers


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = models.SyncKey.objects.filter(Q(key=key) | Q(description__icontains=key)).order_by('-id')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.SynckeyListSerializer(instance=list_page, many=True)
    data['code'] = 0
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)