# coding:utf-8
from django.http import JsonResponse
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from . import models, serializers
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from Base.Functions import getuserip


# Create your views here.
@api_view(['GET'])
def echo_clean(request, key):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    if user.is_superuser:
        models.EchoLog.objects.filter(key__icontains=key).delete()
        data['code'] = 0
        data['msg'] = '清理完成'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['GET'])
def main_list(request, key):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.EchoLog.objects.filter(key__icontains=key).order_by('-create_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.EchoLogSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def key_log(request, key):
    data = {'code':0, 'msg':'success'}
    req_path = request.get_full_path()
    ip = getuserip.get_client_ip(request)
    ua = request.META.get('HTTP_USER_AGENT')
    models.EchoLog.objects.create(
        key=key,
        path=req_path,
        ip=ip,
        ua=ua,
    )
    return JsonResponse(data)
