# coding:utf-8

from django.http import JsonResponse
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from datetime import datetime
from django.views.decorators.csrf import csrf_protect
from rest_framework.decorators import api_view


# Create your views here.
@api_view(['GET'])
def mainlist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    starttime = request.GET.get('starttime', '1999-1-1')
    endtime = request.GET.get('endtime', datetime.now())
    list_get = models.Building_Projects.objects.filter(name__icontains=key,
                                         updatetime__gte=starttime,
                                         updatetime__lte=endtime).order_by('-updatetime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.BuildingProjectSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def level_list(request):
    data = {
        "code": 0,
        "msg": "",
        "data": []
    }
    list_get = models.Building_Projects_LEVEL.objects.all().order_by('id')
    serializers_get = serializers.BuildingProjectsLEVELSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def project_create(request):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    form = forms.BuildingProjectsUpdateForm(request.POST)
    if form.is_valid():
        form.save()
        data['code'] = 0
        data['msg'] = '创建成功'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def project_update(request, project_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    project_get = models.Building_Projects.objects.filter(id=project_id).first()
    if project_get:
        form = forms.BuildingProjectsUpdateForm(request.POST, instance=project_get)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = '创建成功'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def project_delete(request, project_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    project_get = models.Building_Projects.objects.filter(id=project_id).first()
    if project_get:
        project_get.delete()
        data['msg'] = '删除成功'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def project_action_create(request, project_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    project_get = models.Building_Projects.objects.filter(id=project_id).first()
    if project_get:
        form = forms.BuildingProjectsActionForm(request.POST)
        if form.is_valid():
            models.Building_Projects_Action.objects.create(project=project_get, action=form.cleaned_data['action'], person = form.cleaned_data['person'], actiontime=form.changed_data['actiontime'])
            data['code'] = 0
            data['msg'] = '记录完成'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)

