# -*- coding: utf-8 -*-
# @Time    : 2020/7/23
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : vulnfun.py

from .. import models
from Workflow.Functions import workflowfun, ticketfun
from Workflow import models as workflowmodels
from django.db.models import Q


def get_vuln_list(user_get):
    list_get = models.Vuln.objects.none()
    if user_get.is_superuser:
        list_get = models.Vuln.objects.all()
    return list_get


def check_vuln_permission(vuln_id, user_get):
    if user_get.is_superuser:
        item_get = models.Vuln.objects.filter(id=vuln_id).first()
        return item_get
    return False


def vuln_create(data_get, user_obj, is_correction):
    """
    传入的数据如为外键属性，需为有效obj
    """
    flag = False
    if is_correction:
        # 如果校验过，不做任何修改
        pass
    else:
        # 如果没校验，则进行过滤
        advance_vuln = models.AdvanceVuln.objects.filter(
            name=data_get.get('name'),
            source=data_get.get('source')
        )
        if advance_vuln:
            data_get['type'] = advance_vuln.type
            data_get['level'] = advance_vuln.level
            data_get['introduce'] = advance_vuln.introduce
            data_get['fix'] = advance_vuln.fix
            data_get['advance_vuln'] = advance_vuln
    vuln_get = models.Vuln.objects.get_or_create(
        name=data_get.get('name'),
        type=data_get.get('type'),
        level=data_get.get('level'),
        source=data_get.get('source'),
        introduce=data_get.get('introduce'),
        info=data_get.get('info'),
        fix=data_get.get('fix'),
        asset=data_get.get('asset'),
        advance_vuln=data_get.get('advance_vuln')
    )
    if vuln_get[1]:
        # 创建新的流程并修改漏洞的状态
        vuln_get[0].task = data_get.get('task')
        vuln_get[0].user = user_obj
        vuln_get[0].save()
        res = workflowfun.create_workflow(user_obj, 'vuln', vuln_get[0])
        if res:
            flag = True
        else:
            vuln_get[0].delete()
    else:
        # 重置当前漏洞流程并修改漏洞状态
        ticket_get = workflowmodels.Tickets.objects.filter(ticket_key='vuln', ticket_id=vuln_get[0].id).first()
        if ticket_get:
            # 如果漏洞状态是已修复重新指派漏洞流程
            workflow_get = ticket_get.workflow
            if workflow_get.key == 'vuln_finish':
                ticketfun.workflow_flow(ticket_get, user_obj, '漏洞复现，请检查', False)
                flag = True
        else:
            vuln_get[0].delete()
    return flag


def dic2vuln(vuln_dic, user_obj):
    """
    param vuln_dic:{
                    'name': 'name',
                    'info': 'details',
                    'scope': 'scope',
                    'description': ‘description’,
                    'fix': 'fix',
                    'level': '高危',
                    'source': 'source_key',
                    'task': 'task_obj',
                    'asset': asset_key,
                }
    """
    vuln_dic.source = models.Source.objects.filter(key=vuln_dic['source']).first()
    vuln_dic.level = models.LEVEL.objects.filter(
        Q(key=vuln_dic.get('level')) |
        Q(name=vuln_dic.get('level'))
    ).first()
    vuln_dic.asset = models.Asset.objects.get_or_create(key=vuln_dic['asset'])[0]
    vuln_create(vuln_dic, user_obj, False)
