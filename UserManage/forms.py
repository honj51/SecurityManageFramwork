# -*- coding: utf-8 -*-
# @Time    : 2020/11/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py

from django import forms
from Base.models import Person
from RBAC import models


class UserEditForm(forms.Form):
    username = forms.CharField(label='用户名')
    password = forms.CharField(label='密码')
    title = forms.CharField(label='用户昵称')
    person = forms.ModelChoiceField(queryset=Person.objects.all(), label='关联用户')
    roles = forms.ModelChoiceField(queryset=models.Role.objects.all(), label='关联用户')
    permissionmanage = forms.CharField(label='权限管理',required=False)