# -*- coding: utf-8 -*-
# @Time    : 2020/7/2
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : roles.py

from .. import models, serializers
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import serializers


@api_view(['GET'])
def role_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        list_get = models.Role.objects.filter(name__icontains=key).order_by('id')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.RoleSerializer(instance=list_page, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '无权访问'
    return JsonResponse(data)




