"""SemfPro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.urls.conf import include
from django.conf.urls.static import static

urlpatterns = [
                path('', include('Base.urls')),
                path('log/', include('Log.urls')),
                path('rbac/', include('RBAC.urls')),
                path('usermanage/', include('UserManage.urls')),
                path('vuln/', include('Vuln.urls')),
                path('building/', include('Building.urls')),
                path('task/', include('Task.urls')),
                path('scanner/', include('Scanner.urls')),
                path('asset/', include('Asset.urls')),
                path('echolog/', include('EchoLog.urls')),
                path('poc/', include('Poc.urls')),
                path('workflow/',include('Workflow.urls')),
                path('captcha/', include('captcha.urls')),
                path('api/', include('ApiManage.urls')),
                # path('semfpro/', admin.site.urls),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
