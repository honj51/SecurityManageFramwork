# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : assetviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import models, serializers
from ..Functions import assetfun
from django.db.models import Q
from .. import forms


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    check = request.GET.get('check', '')
    if check:
        list_get = assetfun.get_asset_list(request.user).filter(
            Q(name__icontains=key) |
            Q(key__icontains=key),
            type__isnull=True
        ).order_by('-update_time')
    else:
        list_get = assetfun.get_asset_list(request.user).filter(
            Q(name__icontains=key) |
            Q(key__icontains=key)
        ).order_by('-update_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.AssetSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def select_asset_views(request):
    data = {
        "code": 0,
        "msg": "",
        #"count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = assetfun.get_asset_list(request.user).filter(
        key__icontains=key
    ).order_by('id')[:10]
    # list_count = list_get.count()
    serializers_get = serializers.AssetSelectSelectSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    # data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def details_views(request, asset_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    asset_item = assetfun.check_asset_permission(asset_id, request.user)
    if asset_item:
        data['code'] = 0
        data['msg'] = 'success'
        data['data'] = xssfilter(assetfun.get_asset_details(asset_item))
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def delete_views(request, asset_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_asset_permission(asset_id, request.user)
    if item_get:
        # item_get.is_use = False
        # item_get.save()
        item_get.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    # user = request.user
    form = forms.AssetForm(request.POST)
    if form.is_valid():
        asset_item = models.Asset.objects.get_or_create(
            key=form.cleaned_data['key']
        )
        if asset_item[1]:
            asset_item = asset_item[0]
            asset_item.type = form.cleaned_data['type']
            asset_item.name=form.cleaned_data['name']
            asset_item.description = form.cleaned_data['description']
            asset_item.is_out = form.cleaned_data['is_out']
            asset_item.manage = form.cleaned_data['manage']
            if form.cleaned_data['parent']:
                asset_group_list = models.Group.objects.filter(
                    id__in=form.cleaned_data['parent'].split(','))
                for item in asset_group_list:
                    item.asset.add(asset_item)
            asset_item.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '资产已存在'
    else:
        data['msg'] = '违规输入或资产已存在'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def update_views(request, asset_id):
    data = {
        "code": 1,
        "msg": "",
    }
    # user = request.user
    asset_item = assetfun.check_asset_permission(asset_id, request.user)
    if asset_item:
        form = forms.AssetUpdateForm(request.POST, instance=asset_item)
        if form.is_valid():
            asset_item.name = form.cleaned_data['name']
            asset_item.type = form.cleaned_data['type']
            asset_item.description = form.cleaned_data['description']
            asset_item.is_out = form.cleaned_data['is_out']
            asset_item.manage = form.cleaned_data['manage']
            if form.cleaned_data['parent']:
                asset_group_list = models.Group.objects.filter(
                    id__in=form.cleaned_data['parent'].split(','))
                for item in asset_group_list:
                    item.asset.add(asset_item)
            asset_item.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '不是你的何必强求'
    return JsonResponse(data)
