# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : pluginviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import models, serializers, forms
from ..Functions import assetfun


@api_view(['GET'])
def list_views(request, asset_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    asset_item = assetfun.check_asset_permission(asset_id, request.user)
    if asset_item:
        list_get = models.PluginInfo.objects.filter(asset=asset_item, name__icontains=key).order_by('id')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.PluginInfoSerializer(instance=list_page, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def delete_views(request, plugin_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_plugin_permission(plugin_id, request.user)
    if item_get:
        item_get.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request, asset_id):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.PluginForm(request.POST)
    if form.is_valid():
        item_get = assetfun.check_asset_permission(asset_id, request.user)
        if item_get:
            models.PluginInfo.objects.get_or_create(
                name=form.cleaned_data['name'],
                version=form.cleaned_data['version'],
                plugin_info=form.cleaned_data['plugin_info'],
                asset=item_get
            )
            data['code'] = 0
            data['msg'] = 'success'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def  update_views(request, plugin_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_plugin_permission(plugin_id, request.user)
    if item_get:
        form = forms.PluginForm(request.POST, instance=item_get)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)